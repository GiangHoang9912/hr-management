@extends('layouts.master')
@section('content')
{{-- message --}}
{!! Toastr::message() !!}
<div class="page-wrapper">
<div>
    <h1>
        Các F của tôi
    </h1>

    <!-- button add F member -->
    <button id="addF" onclick="showAddFDialog()">Thêm F</button>
    <!-- show dialog add F with params is DSN -->
    <div id="dialogAddF" class="dialog">
        <div class="dialog-content">
            <span id="close-dialog" class="close">&times;</span>
            <table>
                <tr>
                    <td>Tên F</td>

                    <td>
                        <input type="text" name="name" id="name" value="Nguyễn Văn A">
                    </td>
                </tr>
                <tr>
                    <td>Doanh số 1-1</td>
                    <td>
                        <input type="text" name="valueOneToOne" id="valueOneToOne" value="100000000">
                    </td>
                </tr>
                <tr>
                    <td>Doanh số cá nhân ilets</td>
                    <td>
                        <input type="text" name="valueIlets" id="valueIlets" value="100000000">
                    </td>
                </tr>
                <tr>
                    <td>Doanh số cá hệ thống</td>
                    <td>
                        <input type="text" name="valueSystem" id="valueSystem" value="100000000">
                    </td>
                </tr>
                <tr>
                    <td>Là CEO F2</td>
                    <td>
                        <!-- checkbox -->
                        <input type="checkbox" name="isCEO" id="isCEO" value="1">
                    </td>
                </tr>
                <tr id="indexFTr" style="visibility: hidden;">
                    <td>ID Phó CT</td>
                    <td>
                        <input type="text" name="idPct" id="idPct" value="1">
                    </td>
                </tr>
            </table>
            <!--  button add -->
            <button id="add" onclick="addNewF()">Thêm</button>
        </div>
    </div>

    <table id="list-member">
        <th>id</th>
        <th>Name</th>
        <th>Doanh thu Ilets</th>
        <th>Doanh thu 1-1</th>
        <th>Doanh thu hệ thống</th>
        <th>Doanh thu của tôi từ F này</th>
        <th>Bậc</th>
    </table>

    <div id="caculate">
        <div>
            <table>
                <tr>
                    <td>
                        Họ và tên
                    </td>
                    <td>
                        <input type="text" name="name" id="myName" value="Nguyễn Văn B">
                    </td>
                </tr>
                <tr>
                    <td>
                        DSCN Ilets
                    </td>
                    <td>
                        <input type="text" name="dscnIlets" id="myDscnIlets" value="10000000000">
                    </td>
                </tr>
                <tr>
                    <td>
                        Doanh số Nhóm
                    </td>
                    <td>
                        <input type="text" name="mySystemValue" id="mySystemValue" value="10000000000">
                    </td>
                </tr>
                <tr>
                    <td>
                        DSCN Doanh số 1-1
                    </td>
                    <td>
                        <input type="text" name="myOnetoOneValue" id="myOnetoOneValue" value="10000000000">
                    </td>
                </tr>
                <tr>
                    <td>
                        Bậc
                    </td>
                    <td>
                        <h4 name="myPosition" id="myPosition">
                    </td>
                </tr>
                <tr>
                    <td>
                        salary
                    </td>
                    <td>
                        <h4 name="mySalary" id="mySalary">
                    </td>
                </tr>
            </table>
            <div>
                <button id="caculate" onclick="detectPosition()">Tính</button>
            </div>
        </div>
    </div>

    <div id="explan"></div>
</div>
<!-- /Page Wrapper -->
@section('script')
<script>
const Position_Master = {
    level1: {
        level: 1,
        position: "Tư vấn viên cấp 1",
        raito: 0.18,
        min: 0,
        max: 80000000
    },
    level2: {
        level: 2,
        position: "Tư vấn viên cấp 2",
        raito: 0.2,
        min: 80000000,
        max: 400000000
    },
    level3: {
        level: 3,
        position: "Trưởng phòng",
        raito: 0.22,
        min: 400000000,
        max: 800000000
    },
    level4: {
        level: 4,
        position: "Giám đốc vùng",
        raito: 0.24,
        min: 800000000,
        max: 1600000000
    },
    level5: {
        level: 5,
        position: "Giám đốc kinh doanh cấp cao",
        raito: 0.26,
        min: 1600000000,
        max: 3200000000
    },
    level6: {
        level: 6,
        position: "Cổ đông chiến lược",
        raito: 0.28,
        min: 3200000000,
        max: 6400000000
    },
    level7: {
        level: 7,
        position: "CEO",
        raito: 0.29,
        min: 6400000000,
        max: Number.MAX_VALUE
    },
    level8: {
        level: 8,
        position: "Phó CT",
        raito: 0.29,
        min: 6400000000,
        max: Number.MAX_VALUE
    },
    level9: {
        level: 9,
        position: "Co-Founder",
        raito: 0.29,
        min: 32000000000,
        max: Number.MAX_VALUE
    },
}

const conditionOfValue = 50000000;

class Member {
    constructor(name, valueOneToOne, valueIlets, valueOfSystem, idPct = 0) {
        this.id = listF.length + 1;
        this.name = name;
        this.valueOneToOne = valueOneToOne;
        this.valueIlets = valueIlets;
        this.valueOfSystem = valueOfSystem
        this.title = "";
        this.current_position = null;
        this.idPct = idPct;
        this.setPosition();
    }

    setPosition() {
        switch (true) {
            case this.valueOfSystem > Position_Master.level1.min && this.valueOfSystem <= Position_Master.level1.max:
                this.current_position = Position_Master.level1;
                break;
            case this.valueOfSystem > Position_Master.level2.min && this.valueOfSystem <= Position_Master.level2.max:
                this.current_position = Position_Master.level2;
                break;
            case this.valueOfSystem > Position_Master.level3.min && this.valueOfSystem <= Position_Master.level3.max:
                this.current_position = Position_Master.level3;
                break;
            case this.valueOfSystem > Position_Master.level4.min && this.valueOfSystem <= Position_Master.level4.max:
                this.current_position = Position_Master.level4;
                break;
            case this.valueOfSystem > Position_Master.level5.min && this.valueOfSystem <= Position_Master.level5.max:
                this.current_position = Position_Master.level5;
                break;
            case this.valueOfSystem > Position_Master.level6.min && this.valueOfSystem <= Position_Master.level6.max:
                this.current_position = Position_Master.level6;
                break;
            case this.valueOfSystem > Position_Master.level7.min && this.valueOfSystem <= Position_Master.level7.max:
                this.current_position = Position_Master.level7;
                break;
            case this.valueOfSystem > Position_Master.level8.min && this.valueOfSystem <= Position_Master.level8.max:
                this.current_position = Position_Master.level8;
                break;
            case this.valueOfSystem > Position_Master.level9.min && this.valueOfSystem <= Position_Master.level9.max:
                this.current_position = Position_Master.level9;
                break;
        }

        // check from lv > 3
        if (this.current_position && this.current_position.level >= 3) {
            // check value of ilets
            if (this.valueIlets > conditionOfValue) {
                this.title = this.current_position.position;
            } else {
                if (this.valueIlets + this.valueOneToOne < conditionOfValue) {
                    // title = current level - 1
                    this.title = Position_Master[`level${this.current_position.level - 1}`].position;
                    this.current_position = Position_Master[`level${this.current_position.level - 1}`];
                }
            }
            if (this.current_position.level == 7) {
                // role PCT check xem có nhánh CEO không trong listF
                const isCEO = listF.filter(f => {
                    if (f.idPct === this.id) {
                        return f;
                    }
                });

                if (isCEO.length > 0 && isCEO.length < 3 && this.valueOfSystem > Position_Master.level8.min && this.valueOfSystem <= Position_Master.level8.max) {
                    this.current_position = Position_Master.level8;
                    this.title = this.current_position.position;
                } else if (isCEO.length >= 3 && this.valueOfSystem > Position_Master.level9.min && this.valueOfSystem <= Position_Master.level9.max) {
                    this.current_position = Position_Master.level9;
                    this.title = this.current_position.position;
                }
            }
        } else {
            this.title = this.current_position.position;
        }

        return this.current_position;
    }
}

// get listF from local storage
let listF = [];

listF.push(new Member("Lê Thành Quý 1", 6500000000, 6500000000, 6500000000))
listF.push(new Member("Lê Thành Quý 2", 6500000000, 6500000000, 6500000000))
listF.push(new Member("Lê Thành Quý 3", 6500000000, 6500000000, 6500000000))
listF.push(new Member("Lê Thành Quý 4", 6500000000, 6500000000, 6500000000, 1))
listF.push(new Member("Đỗ Thị Thanh An", 33436000, 33436000, 33436000))

listF.reverse();

const showAddFDialog = () => {
    // show dialog addF
    document.getElementById("dialogAddF").style.display = "block";

    document.getElementById("close-dialog").addEventListener("click", () => {
        document.getElementById("dialogAddF").style.display = "none";
    })
}

const showListF = () => {
    // clear table data tr
    const table = document.getElementById("list-member");
    for (let i = table.rows.length - 1; i > 0; i--) {
        table.deleteRow(i);
    }

    for (const f of listF) {
        if (f) {
            const table = document.getElementById("list-member");
            const row = table.insertRow(1);
            row.insertCell(0).innerHTML = f.id;
            row.insertCell(1).innerHTML = f.name;
            row.insertCell(2).innerHTML = f.valueOneToOne.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            row.insertCell(3).innerHTML = f.valueIlets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            row.insertCell(4).innerHTML = f.valueOfSystem.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            row.insertCell(5).innerHTML = ``
            row.insertCell(6).innerHTML = f.title;
            // btn delete
            row.insertCell(7).innerHTML = `<button class="btn btn-danger" onclick="deleteF(${f.id})">Xóa</button>`;
        }
    }
}

document.addEventListener("mousemove", () => {
    for (const f of listF) {
        f.setPosition();
    }
    showListF();
})

const deleteF = (id) => {
    const currentList = []
    listF.map(f => {
        console.log(f)
        if (f && f.id !== id) {
            currentList.push(f)
        }
    })

    listF = currentList;

    showListF()
}



// add new F
const addNewF = () => {
    const name = document.getElementById("name").value;
    const valueOneToOne = document.getElementById("valueOneToOne").value;
    const valueIlets = document.getElementById("valueIlets").value;
    const valueSystem = document.getElementById("valueSystem").value;
    const idPct = document.getElementById("idPct").value;

    const f = new Member(name, Number(valueOneToOne.replaceAll(",", "")), Number(valueIlets.replaceAll(",", "")), Number(valueSystem.replaceAll(",", ""), Number(idPct)));
    listF.push(f);
    document.getElementById("dialogAddF").style.display = "none";

    // add to display table member f
    const table = document.getElementById("list-member");
    const row = table.insertRow(1);
    row.insertCell(0).innerHTML = f.id;
    row.insertCell(1).innerHTML = f.name;
    row.insertCell(2).innerHTML = f.valueOneToOne.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    row.insertCell(3).innerHTML = f.valueIlets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    row.insertCell(4).innerHTML = f.valueOfSystem.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    row.insertCell(5).innerHTML = f.title;
    // btn delete
    row.insertCell(6).innerHTML = `<button class="btn btn-danger" onclick="deleteF(${f.id})">Xóa</button>`;

    showListF();
}

const myName = document.getElementById("myName");
const dscnIlets = document.getElementById("myDscnIlets");
const mySystemValue = document.getElementById("mySystemValue");
const myOnetoOneValue = document.getElementById("myOnetoOneValue");
const isCEOF2 = document.getElementById("isCEO");

const detectPosition = () => {
    // dscnIlets onchange => detect position
    const dscnIletsValue = Number(dscnIlets.value.replaceAll(",", ""));
    const valueOfSystem = Number(mySystemValue.value.replaceAll(",", ""));
    const valueOneToOne = Number(myOnetoOneValue.value.replaceAll(",", ""));


    if (dscnIletsValue && valueOfSystem && valueOneToOne) {
        const myPosition = new Member(myName.value, valueOneToOne, dscnIletsValue, valueOfSystem);
        // show position onchange value

        if (myPosition.current_position.level == 7) {
            // role PCT check xem có nhánh CEO không trong listF
            const isCEO = listF.filter(f => f && f.current_position.level == 7);
            if (isCEO.length > 0 && isCEO.length < 3) {
                // hạ lv8 xuống 7
                myPosition.current_position = Position_Master.level8;
            } else if (isCEO.length >= 3) {
                // hạ lv9 xuống 8
                myPosition.current_position = Position_Master.level9;
            }
        }

        myPosition.title = myPosition.current_position.position

        const myPositionNode = document.getElementById("myPosition");
        myPositionNode.textContent = myPosition.title;

        // compute salary
        const salary = document.getElementById("mySalary");
        computeSalary(myPosition, salary);
    }
}

const computeSalary = (myPosition, salary) => {
    let textExplain = `
    <p>level ${myPosition.current_position.level}: ${(myPosition.current_position.raito * 100).toFixed(0)}%</p>
    <p>Giải thích: </p>
    `
    const currentArrayListF = [...listF]
    if (myPosition.current_position.level < 8) {
        let totalSalaryFromF = 0;
        // salary of me from listF = sum((myPosition.current_position.raito-raito of F)DSCN Ilets of F in listF)
        for (const f of currentArrayListF) {
            const salaryFromF = (myPosition.current_position.raito - f.current_position.raito) * f.valueIlets;
            totalSalaryFromF += salaryFromF;
            textExplain += `
            <p>lương của ${f.name} = (${myPosition.current_position.raito} - ${f.current_position.raito}) * ${f.valueIlets} = ${salaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
            `
        }
        textExplain += `
        <p>Tổng lương của tôi từ nhánh F = ${totalSalaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
        `

        salary.textContent = (totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        textExplain += `
        <p>Lương của tôi = ${myPosition.current_position.raito} * ${myPosition.valueIlets} + Tổng lương của tôi từ nhánh F = ${(totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
        `
    } else if (myPosition.current_position.level == 8) {
        // salary = 2.5% CEO + 29% DSCN +  SUM(F - CEO)
        let CEOS = currentArrayListF.filter(f => f && f.current_position.level == 7);
        let salaryFromCEO = 0;

        for (const CEO of CEOS) {
            if (CEO) {
                salaryFromCEO += CEO.valueIlets * 0.025;

                textExplain += `
                <p>lương của ${CEO.name} = ${CEO.valueIlets} * 2.5% = ${(CEO.valueIlets * 0.025).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                `

                // delete CEO from listF
                currentArrayListF.splice(currentArrayListF.indexOf(CEO), 1);
            }
        }

        let totalSalaryFromF = 0;

        // salary of me from listF = sum((myPosition.current_position.raito-raito of F)DSCN Ilets of F in listF)
        for (const f of currentArrayListF) {
            if (f) {
                const salaryFromF = (myPosition.current_position.raito - f.current_position.raito) * f.valueIlets;
                totalSalaryFromF += salaryFromF;
                textExplain += `
                <p>lương của ${f.name} = (${myPosition.current_position.raito} - ${f.current_position.raito}) * ${f.valueIlets} = ${salaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                `
            }
        }

        textExplain += `
        <p>Tổng lương của tôi từ nhánh F = ${totalSalaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
        `

        salary.textContent = (salaryFromCEO + totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        textExplain += `
        <p>Lương của tôi = 2.5% CEO + Tổng lương của tôi từ nhánh F + ${myPosition.current_position.raito} * ${myPosition.valueIlets} = ${(salaryFromCEO + totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
        `
    } else if (myPosition.current_position.level == 9) {
        // (3% DSN PCT + 0.5% DSN CEO F2) + ( 3% DSN CEO F1 ) + 29% DSCN + SUM(Doanh số tính theo F1)
        const PCTS = currentArrayListF.filter(f => f && f.current_position.level == 8);
        let salaryFromPCT = 0;
        let salaryFromCEOF2 = 0;
        console.log(PCTS)

        for (const PCT of PCTS) {
            // 2.5% DSN CEO + 29% DSCN + SUM(Doanh số tính theo F1)
            // CEOS = listF.filter(f => f.current_position.level == 7);
            console.log(PCT)
            // DSNPCT = PCT.valueIlets - DSN CEOF2 of PCT
            const CEOF2 = listF.filter(f => f && f.current_position.level == 7 && f.idPct == PCT.id);

            if (CEOF2.length > 0) {
                CEOF2.forEach(f => {
                    salaryFromCEOF2 += f.valueIlets * 0.005;
                    textExplain += `
                    <p>Lương của tôi từ F2 là CEO nhánh Phó CT = f.valueIlets * 0.005 = ${f.valueIlets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} * 0.005 = ${salaryFromCEOF2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                    `
                    // delete CEOF2 from listF
                    currentArrayListF.splice(currentArrayListF.indexOf(f), 1);
                })
            }
            salaryFromPCT += PCT.valueIlets * 0.03;
            textExplain += `
            <p>Lương của tôi từ nhánh phó CT = PCT.valueIlets * 0.03 = ${PCT.valueIlets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} * 0.03 = ${salaryFromPCT.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
            `
            // delete PCT from listF
            currentArrayListF.splice(currentArrayListF.indexOf(PCT), 1);
        }


        const CEOS = currentArrayListF.filter(f => f && f.current_position.level == 7);

        console.log(CEOS)
        let salaryFromCEOLV9 = 0;

        for (const CEO of CEOS) {
            if (CEO) {
                salaryFromCEOLV9 += CEO.valueIlets * 0.03;
                // delete CEO from listF
                textExplain += `
                <p>Lương của tôi từ nhánh CEO F1 = CEO.valueIlets * 0.03 = ${CEO.valueIlets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} * 0.03 = ${salaryFromCEOLV9.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                `
                currentArrayListF.splice(currentArrayListF.indexOf(CEO), 1);
            }
        }

        let totalSalaryFromF = 0;

        for (const f of currentArrayListF) {
            if (f) {
                const salaryFromF = (myPosition.current_position.raito - f.current_position.raito) * f.valueIlets;
                totalSalaryFromF += salaryFromF;
            }
        }

        textExplain += `
        <p>Lương của tôi từ các nhánh còn lại = ${totalSalaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p> <br />
        <p>% Doanh số cá nhân = ${myPosition.current_position.raito} * ${myPosition.valueIlets.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} = ${(myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>`

        salary.textContent = (salaryFromPCT + salaryFromCEOF2 + salaryFromCEOLV9 + totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
        textExplain += `
        <p>Lương của tôi = (3% DSN PCT + 0.5% DSN CEO F2) + ( 3% DSN CEO F1 ) + 29% DSCN + SUM(Doanh số tính theo F1) + % Doanh số cá nhân = ${salaryFromPCT.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} + ${salaryFromCEOF2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} + ${salaryFromCEOLV9.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} + ${totalSalaryFromF.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} + ${(myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} = ${(salaryFromPCT + salaryFromCEOF2 + salaryFromCEOLV9 + totalSalaryFromF + myPosition.current_position.raito * myPosition.valueIlets).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
        `
    }


    const explanation = document.getElementById("explan");
    explanation.innerHTML = textExplain
}

myName.addEventListener("change", () => {
    detectPosition();
});

dscnIlets.addEventListener("change", () => {
    detectPosition();
});

mySystemValue.addEventListener("change", () => {
    detectPosition();
});

myOnetoOneValue.addEventListener("change", () => {
    detectPosition();
});

// checkbox to show div
isCEOF2.addEventListener("change", () => {
    const divF2 = document.getElementById("indexFTr");
    if (isCEOF2.checked) {
        divF2.style.visibility = "visible";
    } else {
        divF2.style.visibility = "hidden";
    }
})
</script>
@endsection
@endsection